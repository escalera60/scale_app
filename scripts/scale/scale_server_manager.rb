
#!/usr/bin/env ruby
require 'rubygems'
require 'net/ssh'
require 'net/http'
require 'net/https'
require 'net/telnet'
require 'net/scp'
require 'net/ssh/telnet'
require 'fileutils'
require 'socket'
require 'yaml'
require 'nokogiri'
require 'timeout'
#require 'expectcls.rb'
require 'time'
require 'zlib'
require 'rubygems/package'
require 'pony'
require 'exceptions'

$ip_list = '192.169.10.101'

class RaspberryPi_Shell


    # Envoy_Shell constructor:
    # Will connect via SSH to the Emu/Envoy.
    #
    # Parameters:
    # * <b>params</b> | Hash | = <i>A Hash containing the following:</i>
    # * <b>:address</b> | String | = <i>The address of the Envoy.</i>
    # * <b>:verbose</b> | True/False | = <i>If true, will print interaction to stdout.</i>
    # * <b>:username</b> | String | = <i>The login username.  Optional, with default as 'root'.</i>
    # * <b>:pw</b> | String | = <i>The login password.  Optional, with default as '1sunpit4u'. However, if the password fails, will try the hashed password.</i>
    # * <b>:log</b> | Report | = <i>The object that handles logging all test result and data.</i>
    # * <b>:telnet</b> | String | = <i>The IP address and port of the device that can handle a telnet connection to the Envoy.</i>
    #
    # Returns:
    # * <b>Envoy_Shell</b> | Envoy_Shell | = <i>The handler for sending command line commands to the Envoy.</i>



    # @rbp_shell = RaspberryPi_Shell.new({:address => device, :verbose => options.verbose, :username => "root",
    #                                    :password => "1sunpit4u", :results => @results, :useTelnet => options.useTelnet})


    def initialize(params)
        @verbose = params[:verbose] || false
        @address = params[:address]
        @username = params[:username] || "root"
        @pw = params[:password] || "oscar"

        #@log = params[:log]
        @telnet = params[:useTelnet]

        @isConnected = false
        @reconnectRetry = true

       # @report = {}

       @prompt = /pi@raspberrypi:~$*/
        #@prompt = /Raspbian/
        @history = []

        @ssh = __connectSSH()
        @scp = __connectSCP()
    end

    def __connectSCP(pw = @pw)

        login = true

        if(!@sshSession)
            ## Try hashed password then the default password
            begin
                #pwh = __getHashPw()
                @sshSession = Net::SSH.start(@address, @username, :password => pw)
            rescue
                @sshSession = Net::SSH.start(@address, @username, :password => pw)
            end
        end
        return @sshSession.scp
    end
# Connects to the Raspberry Pi using SSH.
#
# Parameters:
# * <b>pw</b> | String | = <i>The password to use for the SSH connection.</i>
#
# Returns:
# * <b>s</b> |SSH|Telnet| = <i>The SSH handle, or the Telnet handle if opted to do so in the constructor (@telnet).</i>
    def __connectSSH(pw = @pw)

        if @verbose then
            puts "Creating connection to Raspberry Pi #{@address}"
        end
        login = false
        telnetlog = "/home/escalera"
        # if (@log)
        #     telnetlog = @log.getLogFileStdoutPath() + '/terminal.log'
        # end
        timeout = 20
        ## Try hashed password then the default password
        begin

            if @telnet == "A"
                puts "hello"
                host = @telnet.split(":")[0]
                port = @telnet.split(":")[1]
                port = 22
                puts "HOST: #{host}"
                puts "PORT: #{port}"

                config = {"Host" => host,
                          "Port" => port,
                          "Timeout" => timeout,
                          "Prompt" => @prompt
                }
                config["Output_log"] = telnetlog if telnetlog

                puts "Conf: #{config}"
                #s = Net::Telnet::new(config)

                s = Net::Telnet::new('Host' => '192.168.10.101', 'Port' => 22, 'Telnetmode' => false)
                begin
                    s.puts("")
                    #s.waitfor("Match" => @prompt, "Timeout" => 20) #wait 10 seconds for prompt
                    r = s.waitfor("Match" => 'Raspbian', "Timeout" => 20)
                    puts r

                    s.puts("exit")
                    puts @username
                    puts pw
                    s.login(@username, pw)
                rescue
                    s.puts("")
                    s.login(@username, pw)
                end

            else

                #puts pw
                # @sshSession = Net::SSH.start(@address, @username, :password => pw) do |ssh|
                #     puts ssh
                #     result = ssh.exec!("ls -l")
                #     puts result
                # end
                @sshSession = Net::SSH.start(@address, @username, :password => pw)
                #result = @sshSession.exec!("ls -l")
                #puts result
                #puts "sshSession #{@sshSession}"
                config = {"Session" => @sshSession,
                          "Timeout" => timeout,
                          "Prompt" => @prompt
                }
                config["Output_log"] = telnetlog if telnetlog

                #s = Net::SSH::Telnet.new(config)

               #puts "ssh connection: #{s}"
            end

            login = true

        rescue => exception
            puts exception.message
            puts exception.backtrace
            @sshSession = Net::SSH.start(@address, @username, :password => pw)

            config = {"Session" => @sshSession,
                      "Timeout" => timeout,
                      "Prompt" => @prompt
            }
            config["Output_log"] = telnetlog if telnetlog

            #puts "Config #{config}"
            #s = Net::SSH::Telnet.new(config)
            #
            login = true
            @pw = pw

        end

        if !login
            puts "Connection failed to " + @address.to_s() + " With password: " + @pw.to_s
            @isConnected = false
        else
            if @verbose then
                puts "Using password: " + @pw.to_s + ": for shell #{@address}"
            end
            @isConnected = true
        end


        return s
    end

# Verifies file is a directory.
#
# Parameters:
# * <b>filePath</b> | true | false | = <i>The file being searched for.</i>
#
# Returns:
# * <b>result</b> | true | false | = <i>True if file is a directory, false if it does not exist or is not directory.</i>
    def fileIsDirectory?(filePath)
        output = sendCommand("find #{filePath} -type d")
        if output == nil or output.chomp.include?("No such file or directory")
            return false
        else
            return true
        end
    end

# Verifies file exists.
#
# Parameters:
# * <b>filePath</b> | true | false | = <i>The file being searched for.</i>
#
# Returns:
# * <b>result</b> | true | false | = <i>True if file exists, false if file not found.</i>
    def fileExists?(filePath)
        output = @sshSession.exec!("find #{filePath}")
        #output = sendCommand("pwd")
        #output = sendCommand("find #{filePath}", true)
        #puts "Output for fileExists #{output}"
        if output == nil or output.chomp.include?("No such file or directory")
            return false
        else
            return true
        end
    end

    def scpPull(raspberryPiPath, localPath)
        allowedMinutes = 5
        #check to see if file exists on the Raspberry Pi
        if fileIsDirectory?(raspberryPiPath) then Pathname.new(localPath).mkpath end#Dir.mkdir(path) end
        Timeout::timeout(allowedMinutes*60) do
            #puts "File directory exists"
            begin
                if(fileExists?(raspberryPiPath))
                    begin
                        if fileIsDirectory?(raspberryPiPath)
                            if @verbose then puts "pulling directory #{raspberryPiPath}" end
                            @scp.download!(raspberryPiPath, localPath, :recursive => true)
                        else
                            if @verbose then puts "pulling file #{raspberryPiPath}" end
                            @scp.download!(raspberryPiPath, localPath)
                        end
                    rescue => exception
                        puts $!
                    end
                    # else
                    #   puts "File does not exist: #{envoyPath}"
                end
            rescue Timeout::Error
                puts "Timed out after #{allowedMinutes} attempting to pull file: #{raspberryPiPath} from Raspberry Pi #{@address}"
                raise EnvoyShellNoResponse, "Timed out after #{allowedMinutes} attempting to pull file: #{envoyPath} from envoy #{@address}"
            end
        end
    end
# Sends a command and returns the output as a string.
#
# Parameters:
# * <b>command</b> | String | = <i>The command to pass in.</i>
# * <b>verbose</b> | true | false | = <i>True or false value.  If true will print interaction to stdout.</i>
#
# Returns:
# * <b>output2</b> | String | = <i>The string output from the command.</i>
    def sendCommand(command, verbose = true)
        output2 = nil
        command = "\\\n" + command.strip

        begin
            if @verbose == true and verbose == true
                puts "Unix Command: #{command}"
            end

            @history.unshift({:time => Time.now, :cmd => command})

            #adding timeout because ssh.exec will wait forever if it fails to get a response
            #Currently set to 7 min timeout
            output = nil
            Timeout::timeout(10*60) {

                #puts "@ssh: #{@sshSession}"
                output = @sshSession.exec!(command)

                #puts "output before stripping: #{output}"

                output = output.split(/\n/)

                if output.length > 3
                    output2 = output[2..(output.length - 2)]
                    output2 = output2.join("\n")
                else
                    output2 = ""
                end

                if output2 == ""
                    output2 = nil
                end
            }

        rescue => e
            puts "#{e} Failed to get a response from #{@address} for #{command}"
            raise Exceptions::EnvoyShellNoResponse, "STOPPED TEST: No response for Pi command #{command}"
            # __reconnect
        end

        if (@verbose == true and verbose == true) and output2 != nil
            puts "Pi Driver Command: #{command}"
            puts "Pi Driver Output:  #{output2}"
        end
        return output2
    end



    def die!()
        if @verbose then puts "Closing connection Raspberry Pi #{@address}" end
        begin
            @sshSession.close
        rescue => exception
            puts "Unable to close ssh/scp session for #{@address}, why?"
            puts $!
        end
    end

    def backupDb
        username = 'root'
        puts "\nBacking up Scale database on Rasperry Pi ... "
        #command = "mysqldump --all-databases --single-transaction --user=#{username} --password=#{@pw} > all_databases.sql"
        command = "mysqldump  --user=#{username} --password=#{@pw} scaledb > scaledb.sql"
        result =  sendCommand(command)
        return result
    end

    def scpDb

        puts "Transfering Scale database to local computer ..."
        nestdb_file     = "/home/pi/scaledb.sql"
        local_directory = HOME + "/databases/"
        scpPull(nestdb_file, local_directory)

    end

    def restoreDb
        username = 'root'
        puts "Refreshing Scale Database on local computer ..."
        value = %x(mysql --user=#{username} -p#{@pw} scaledb < #{HOME}/databases/scaledb.sql 2>/dev/null)
        #value = %x(mysql  --login-path=local nestdb < #{HOME}/databases/nestdb.sql 1>/dev/null)
    end

end

class Email

    # working on this........
    # Initialize the smtp options in order to send email.  There are two instances:
    # * If we are using Cruise then we use the local enphase smtp server.  The reason for this is that only the local enphase
    # smtp server can send email to mailing list.  However, you can only access the smtp server from selected ip addresses (ie Cruise server)
    # * If we are running locally (on Mac, etc) then we use the gmail smtp server.  With the gmail smtp you can send email from any machine,
    # only to direct email receipients, and not email lists.
    def initialize()
        ip_list = [$ip_list]
        @other_mail = Base64.strict_decode64(CONFIG['energy_plan'])
        get_ip = '192.168.10.101'
        begin
            ip = get_ip
        rescue
            ip = "default"
        end

        if ip_list.include?(ip)
            @mail_type = 'smtp'
            @from_address = 'eric_scalera@gmail.com'
            @options = {
                :address => 'smtp.gmail.com',
                :port => 587,
                #:domain => 'hotmail.:enable_starttls_auto => true,
                :user_name            => 'eric.scalera@gmail.com',
                :password             => 'password_see_note',
                :authentication       => :plain, # :plain, :login, :cram_md5, no auth by default,
                :domain               => "localhost.localdomain" # the HELO domain provided by the client to the servercom',

            }
        else
            @mail_type = 'smtp'
            @from_address = 'Eric Scalera <eric_scalera@hotmail.com>'
            @options = {
                :enable_starttls_auto => true,
                #:address => 'smtp-mail.outlook.com',
                :address => 'smtp.sendgrid.net',
                :port => 587,
                :user_name => 'escalera',
                :password => @other_mail,
                :authentication => :plain,
                :domain => 'hotmail.com',


            }
        end
    end


    private
    # Private method to get the attachment string to send an email.
    #
    # Parameters:
    # * <b>attachments</b> | Array | = <i>list of attachments (each must include full path).</i>
    #
    # Returns:
    # * <b>attachString</b> | Hash | = <i>The attachment string to use in the send command.</i>
    def getAttachString(attachments)
        attach_string = {}
        num_attach = attachments.length
        file1, file2, file3, file4, file5 = ""

        begin

            if num_attach > 0
                file1 = Pathname.new(attachments[0]).basename
                attach_string = {file1.to_s => File.read(attachments[0])}
            end
            if num_attach > 1
                file2 = Pathname.new(attachments[1]).basename
                attach_string = {file1.to_s => File.read(attachments[0]), \
                file2.to_s => File.read(attachments[1])}
            end
            if num_attach > 2
                file3 = Pathname.new(attachments[2]).basename
                attach_string = {file1.to_s => File.read(attachments[0]), \
                file2.to_s => File.read(attachments[1]), \
                file3.to_s => File.read(attachments[2])}
            end
            if num_attach > 3
                file4 = Pathname.new(attachments[3]).basename
                attach_string = {file1.to_s => File.read(attachments[0]), \
                file2.to_s => File.read(attachments[1]), \
                file3.to_s => File.read(attachments[2]), \
                file4.to_s => File.read(attachments[3])}
            end
            if num_attach > 4
                file5 = Pathname.new(attachments[4]).basename
                attach_string = {file1.to_s => File.read(attachments[0]), \
                file2.to_s => File.read(attachments[1]), \
                file3.to_s => File.read(attachments[2]), \
                file4.to_s => File.read(attachments[3]), \
                file5.to_s => File.read(attachments[4])}
            end

        rescue
            attach_string = {}
            puts "Path to Attachments incorrect, or do not exist"
            puts $!
        end

        return attach_string
    end


    public
    # Sends an email.
    #
    # Parameters:
    # * <b>toList</b> | Array | = <i>The address or addresses to send the email to (if more than one, must be a list type).</i>
    # * <b>sub</b> | String | = <i>The Email subject.</i>
    # * <b>message</b> | String | = <i>The body of the message.</i>
    # * <b>attachments</b> | Array | = <i>optional attachments (needs to be complete paths, including file namse in the form of a list).</i>
    #
    # Returns:
    # * <b>output</b> |Mail| = <i>optional attachments (needs to be complete paths, including file name in the form of a list).</i>
    def send(to_list, sub, message, attachments = [])

        puts "Sending email to: #{to_list.inspect}"
        attach_string = getAttachString(attachments)
        Pony.mail(:to => to_list, :via => :smtp, :via_options => @options, :from => @from_address, :subject => sub, :html_body => message,
                  :attachments => attach_string)
    end
end


def dataBaseRefresh
    other = Base64.strict_decode64(CONFIG['energy_plan'])

    instance = RaspberryPi_Shell.new({:verbose => false, :address => "192.168.10.101", :username => "pi", :password => other, :useTelnet => "192.168.10.101"})

    results =  instance.backupDb
    results =  instance.scpDb
    results =  instance.restoreDb

end

