

    class ScalePlotHolder

        #--------------------------------------------------------
        # => Building plotting parameters
        #--------------------------------------------------------

        def self.get_bar_plot_parameters

            return @@bar_parameters,@gbar
        end

        def self.get_line_plot_parameters


            return @@line_parameters,@gline
        end

        #--------------------------------------------------------
        # => Building plotting parameters
        #--------------------------------------------------------
        gbar = Gruff::Bar.new(900)
        @gbar = gbar

        gline = Gruff::Line.new(900)
        @gline = gline
        #--------------------------------------------------------
        # Gruff bar plotting parameters
        #--------------------------------------------------------
        @@bar_parameters =  [

        # Note to change the x axis label position go to the base.rb griff file and change the draw_axis_labels method.
        gbar.x_axis_label                = "Daily Weight \n 73 Rock Ridge Road, Fairfax, California",
        gbar.title                       = 'Nokia Body Scale Data',
        #gbar.font                        = '/Library/Fonts/Microsoft/Comic Sans MS',
        gbar.font                        = '/mnt/c/Windows/Fonts/comic.ttf',
        gbar.marker_color                = 'blue',
        gbar.right_margin                = 80.0,
        gbar.left_margin                 = 40.0,
        gbar.x_axis_label_offset         = 8.0,
        gbar.x_axis_bottom_label_offset  = 545,
        gbar.bottom_margin               = 80.0,
        gbar.marker_font_size            = 12.0,
        gbar.legend_font_size            = 13.0,
        gbar.marker_count                = 12.0,
        gbar.show_labels_for_bar_values  = 'weight',
        gbar.label_formatting            = "%.1f",
        gbar.maximum_value               = 175,  # Declare a max value for the Y axis
        gbar.minimum_value               = 150,  # Declare a min value for the Y axis
        gbar.marker_line_color           = 'white',


        gbar.replace_colors(['blue','red','black','orange']),
        gbar.theme_37signals,


        gbar.theme = {   # Declare a custom theme
                      :colors => %w(RoyalBlue3 purple green white red #cccccc), # colors can be described on hex values (#0f0f0f)
                      #:marker_color => 'black', # The horizontal lines color
                      #:marker_shadow_color => 'red',
                      #:background_image => '/Users/escalera/Desktop/tam.png'
                      :background_image => '/home/escalera/tam.png'

        },

        ]
        #--------------------------------------------------------
        # Gruff line plotting parameters for Nest Thermostat plot
        #--------------------------------------------------------

        @@line_parameters = [

        #gline.font                         = '/Library/Fonts/Microsoft/Comic Sans MS',
        gline.font                         = '/mnt/c/Windows/Fonts/comic.ttf',
        gline.marker_color                 = 'blue',
        gline.line_width                   = 0.5,
        gline.maximum_x_value              = 80,
        gline.minimum_x_value              = 40,
        gline.right_margin                 = 80.0,
        gline.left_margin                  = 40.0,
        gline.x_axis_label_offset          = 0,
        gline.x_axis_bottom_label_offset   = 530,
        gline.show_vertical_markers        = true,
        gline.hide_line_markers            = false,
        gline.bottom_margin                = 95.0,
        gline.marker_font_size             = 11.0,
        gline.legend_font_size             = 14,
        gline.marker_count                 = 10.0,
        gline.title                        = "Body Scale Data",
        gline.show_vertical_markers        = false,

        # Note to change the x axis label position go to the base.rb griff file and change the draw_axis_labels method.
        gline.y_axis_increment             = 2,
        gline.colors                       = "green",
        gline.marker_line_color            = 'black',

        gline.theme                        = {  :colors => "black",
                                           #:font_color => "red",
                                           #:font_colors => %w(red),
                                            :marker_color => 'grey85',
                                            :font_weight => 'bold',
                                            :additional_line_colors => 'black',
                                            :background_image => '/home/escalera/tam.png',
                                           },

        gline.replace_colors(['blue','red','black','orange']),
        #g.baseline_value = 60
        #g.baseline_color = "green"
        ]



    end
