$LOAD_PATH.push File.join(File.dirname(__FILE__),"..","scripts/scale")

# -------------------------------------------------------
# Created January 6, 2018
# Author: Eric R.Scalera
# --------------------------------------------------------
require 'rubygems'
require 'withings'
require "awesome_print"
require 'io/console'
require 'mysql'
require 'scale_plot_data'
require 'time'
require 'yaml'
require 'date'
require 'logger'


# require 'highline/import'
#
# def password_prompt(message, mask='*')
#   ask(message) { |q| q.echo = mask}
# end
#
# password = password_prompt('Hey dude choose a password: ')
#
# puts "Your password is '#{password}'
HOME = ENV['HOME']
#scale_ATTEMPTS = 10
INTERVAL_PERIOD = 60
EXECUTION_TIME = 6


CONFIG = YAML.load_file("#{HOME}" + "/config.yml") unless defined? CONFIG
@other = Base64.strict_decode64(CONFIG['energy_plan'])

class Scale_Data < Scale_Plot

  #attr_accessor :scale_hash

    # Configure Scale Object outside Google's Firebase interface
    def initialize(config, other)

        # Go to https://developer.health.nokia.com/partner/dashboard to refresh access tokens
        # encoding:
        key1 = {:val1 => Base64.strict_decode64(CONFIG['o'][0])}
        key2 = {:val2 => Base64.strict_decode64(CONFIG['o'][1])}
        key3 = {:val3 => Base64.strict_decode64(CONFIG['o'][2])}
        key4 = {:val4 => Base64.strict_decode64(CONFIG['o'][3])}

        Withings.consumer_key    = key1[:val1]
        Withings.consumer_secret = key2[:val2]

        # Note Status code: 342 means you have a bad token
        @scale = Withings::User.authenticate('6171531',key3[:val3],key4[:val4])


        # Instantiate mysql object
        @con = Mysql.new('localhost', 'escalera', other, 'scaledb')

        @logging = false
        @scale_base_directory = "/mnt/c/Users/eric_/Desktop" + "/" + "Scale_Data"


    end

    # Method dumps the raw data from the Nest API provided by googles interface.
    def dump_raw_scale_data
        ap @scale.measurement_groups, :raw => false, :color => {:hash => :blue}
    end

    # Method which writes Net data to a MySql database.
    def write_scale_info(record_number)

        begin
            $i = 5 # go back to the last 5 entries
            last_db_date    = ''
            last_scale_date = ''
            dbcon = @con
            scale = get_scale_hash(record_number)

            puts scale

            last_db_date_record = dbcon.query("select date from Scale_Data order by id desc limit 1")

            last_db_date = last_db_date_record.fetch_row[0].to_i
            puts "last database  date: #{last_db_date}"

            last_scale_date = scale['date']
            puts "last bodyscale date: #{last_scale_date}"

            userid = scale['userid']

            if last_db_date == last_scale_date
                puts "No new scale records have been found"
            else
                # check back to the last stored withings scale entries in the event communicatons
                # is lost and the database has not been updated.
                while $i >= 0 do
                    scale = get_scale_hash($i)
                    last_scale_date = scale['date']
                    weight = scale['weight']
                    if last_scale_date > last_db_date
                        puts "Writing weight: #{weight}, date: #{last_scale_date} to database"
                        dbcon.query("INSERT INTO Scale_Data(weight,userid,date) VALUES (#{weight},#{userid},#{last_scale_date})")
                    end
                    $i -= 1
                end
            end

        rescue StandardError => e
            @write_data_attempts += 1
            puts "Error Message    : #{e.message}"
            puts "Recovery Attempt : #{@write_data_attempts}"
            puts e.backtrace.join("\n")
            sleep 5
            write_scale_info(n) if (@write_data_attempts <= SCALE_ATTEMPTS)
        end

        @write_data_attempts = 0

    end

    # Method to get the Scale data to insert into the MySql database.
    def get_scale_hash(n)
        @scale_hash = {}
        begin

            scale = @scale.measurement_groups
            @uid = '6171531'
            @scale_hash = {'weight' => scale[n].weight, 'date' => scale[n].taken_at, 'userid' => @uid}

            logger("#{__method__.to_s}")
            logger("#{scale}")
            logger("#{scale[n].weight},#{scale[n].taken_at},#{@uid}")
            logger("#{@scale.hash}")

        #rescue StandardError => e
        rescue Exception => e
            @scale_data_attempts += 1
            puts "Error Message    : #{e.message}"
            puts "Recovery Attempt : #{@scale_data_attempts}"
            puts e.backtrace.join("\n")
            puts "Scale Object: #{@scale}"
            #sleep 10
            sleep 5
            # puts SCALE_ATTEMPTS
            get_scale_hash(n) if (@scale_data_attempts <= SCALE_ATTEMPTS)
        end
        @scale_data_attempts = 0

        return @scale_hash

    end

    # Method to generate a begin and end epoch based on start data and number of days.
    def get_day_epoch(date, days)

        end_epoch = (days.to_i * 86400)
        begin_epoch = Time.parse(date).to_i
        end_epoch = begin_epoch + end_epoch

        logger("Method: #{__method__.to_s}")
        logger("Epoch Start: #{begin_epoch}")
        logger("Epoch End  : #{end_epoch}")

        return [begin_epoch, end_epoch]

    end


    def logger(message)

         logger       = Logger.new(STDOUT)

        if @logging
         logger.level = Logger::DEBUG
         logger.debug {"#{message}"}
        else
            logger.level = Logger::INFO
            logger.debug {"#{message}"}
        end
    end


    # Method to plot the Scale data using either a bar or line graph.
    def plot_scale_data(location, date, days)

        bar_intervals = set_bar_intervals(date,days)

        if days == nil;
            days = 1
        end

        epoch_date  = get_day_epoch(date, days)
        scale_data  = get_scale_data(location, epoch_date[0], epoch_date[1])
        plot_bar_scale_data(location,scale_data,bar_intervals,date)

    end

    def set_bar_intervals(date,days)

        day_interval_array = []
        epoch_start = Time.parse(date).to_i

        0.upto(days.to_i-1) { |day|

             day_interval_array[day] = [epoch_start,(epoch_start+86400)]
             epoch_start = epoch_start + 86400

        }

        return day_interval_array

    end

    def days_in_month(year, month)
        Date.new(year, month, -1).day
    end

    # Method to send the plot to the user on the email list.  The method also copies the database
    # on the Raspberry Pi and send it to the local computer which is then refreshed using the MySql
    # database on the remote computer.
    def mail_plot(period)

        subdirectory      = ''
        plot_type         = ''
        record_number     = 0
        date              = Time.now.strftime("%Y-%m-01")
        date_of_execution = Time.now
        logger("#{__method__.to_s}")
        logger("#{date_of_execution}")
        location          = "Scale_Data"

        if (period =~ /daily/) == 0
            days = nil
            plot_type = "line"
        else
            if (period =~ /monthly/) == 0
                t1 = Time.now.to_i
                subdirectory =  Time.at(t1).strftime("%Y")
                year         =  subdirectory
                month        =  Time.at(t1).strftime("%m")
                days         = days_in_month(year.to_i, month.to_i)
                write_scale_info(record_number)
                plot_scale_data(location,date,days)
                plot_type    = "bar"
            end
        end

        puts "Mailing the latest Scale Bar Plot for #{date} ..."
        mail_addresses = 'eric.scalera@gmail.com'
        attachments   = ["/mnt/c/Users/eric_/Desktop/Scale_Data/#{subdirectory}/scale-#{plot_type}-plot-#{location}-#{date}.png"]
        plot_type     = plot_type.capitalize
        subject       = "Today\'s Scale #{plot_type} Plot for #{date}"
        body          = "Attached is the body scale monthly results"
        mailer        = Email.new
        mailer.send(mail_addresses, subject, body, attachments=attachments)
    end

end

