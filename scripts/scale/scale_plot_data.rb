# encoding: utf-8
$LOAD_PATH.push File.join(File.dirname(__FILE__), "..", "scripts/core/monitor")
$LOAD_PATH.push File.join(File.dirname(__FILE__),"scripts/scale")

require "awesome_print"
require 'io/console'
require 'mysql'
require 'gruff'
require 'rmagick'
require 'scale_plot_holders'


class Scale_Plot

    attr_accessor :weight, :date, :userid, :id

    def build_file_directory(scale_base_directory)


        scale_year_directory = scale_base_directory + "/" + @year
        if !File.directory?(scale_year_directory)
            puts "Year Directory #{scale_year_directory} does not exist. Creating it now."
            if system("mkdir #{scale_year_directory}")
                puts "Directory #{scale_year_directory} successfully created."
            else
                puts "ERROR: Could not create year directory #{scale_year_directory}."
            end
        else
            puts "Directory #{scale_year_directory} already exists. No need to create it."
        end


        return scale_year_directory
    end

    def get_scale_data(location, begin_date, end_date)

        weight     = []
        date       = []
        id         = []
        userid     = []
        scale_data = []

        ne = @con.query("SELECT * FROM #{location} where (date > #{begin_date}) and (date < #{end_date})")

        n_rows = ne.num_rows

        count = 0
        n_rows.times do
            scale_data[count] = ne.fetch_row
            count += 1
        end

        0.upto(count-1) { |row|
            weight << scale_data[row][1].to_f
            date   << scale_data[row][2].to_i
            id     << scale_data[row][0].to_i
            userid << scale_data[row][3].to_f
        }

        return [weight,date,id,userid]
    end

    # used for Imagick plots
    def plot_bar_scale_data(location,scale_data,bar_intervals,date)


        @year  = date.split('-')[0]

        @interval         = bar_intervals
        scale_plot_object = ScalePlotHolder::get_bar_plot_parameters

        # build file directory and file names
        ym = date
        directory       = build_file_directory(@scale_base_directory)
        filename_prefix = "scale-bar-plot-#{location}-#{ym}"

        # local variables
        hvac_total_min = []
        label = "Weight in Imperial Pounds"

        weight     = scale_data[0]
        date       = scale_data[1]
        id         = scale_data[2]

        day_array_num = 0
        hvac_total_min[day_array_num] = 0
        time_array = []
        weight_array = []
        @interval_count = 0

        0.upto(bar_intervals.size-1) { |day|

            intervals = bar_intervals[day]
            @interval_count = 0
            0.upto(date.size-1) do |i|
                if date[i] > intervals[0].to_i && date[i] < intervals[1].to_i
                    time_array[day] = Time.at(date[i]).strftime("%m-%d-%y")
                    weight_array[day] = weight[i]
                    logger("Method: #{__method__.to_s}")
                    logger("Weight: #{weight[i]}, Day #{Time.at(date[i]).strftime("%m-%d-%y")}")
                end
            end

          if time_array[day] == nil
              time_array[day] = Time.at(intervals[0]).strftime("%m-%d-%y")
              weight_array[day] = 150
          end
        }

        @scale_daily_weights = weight_array
        scale_date = Hash[time_array.each_with_index.map { |value, index| [index, value] }]


        # -----------------------------------
        #  gruff plot values and parameters
        # -----------------------------------
        scale_plot_object[1].data("Scale Daily Weight Total", @scale_daily_weights)
        scale_plot_object[1].y_axis_label = label
        scale_plot_object[1].labels = scale_date
        scale_plot_object[1].write("#{directory}/#{filename_prefix.downcase}_.png")
    end


end

