
$LOAD_PATH.push File.join(File.dirname(__FILE__),"scripts/scale")

require 'scale_plot_data'
require 'scale_get_data'
require 'scale_server_manager'
require 'date'

PLOT        = 'plot'
DUMP        = "dump"
SEND_EMAIL  = "email"
DATA        = "data"
WRITE_TO_DB = "write"

#@instance = RaspberryPi_Shell.new({:verbose => false, :address => "10.50.60.128", :username => "root", :password => "1e6a5109f00d12c6c0bb777d7f33ce89",:useTelnet => "localhost"})

def usage()

    puts "-------------------------------------------------------------------"
    puts " Usage"
    puts "-------------------------------------------------------------------"
    puts "-> ruby #{__FILE__} -h \{this help command\}"
    puts ""
    puts "-> ruby #{__FILE__} action=plot location \"2015-12-31\""
    puts ""
    puts "-> ruby #{__FILE__} action=data retrieve"
    puts ""
    puts "-> ruby #{__FILE__} action=dump [scale]"
    puts ""
    puts "-> ruby #{__FILE__} action=email monthly"
    puts ""
    puts "-> ruby #{__FILE__} action=write"
    puts "-------------------------------------------------------------------"
    puts ""
end

def days_in_month(year, month)
    Date.new(year, month, -1).day
end


def start_test()

    action    = ''
    date      = ''
    location  = ''
    parameter = ''
    period    = ''


    instance = Scale_Data.new(CONFIG,@other)

    #check program arguments for the scale utility
    if ARGV.length == 1|| ARGV.length == 2 || ARGV.length == 3 || ARGV.length == 4

        if ARGV[0] =~ /\-h/
            usage
        end
        action = ARGV[0].split('=')[1] if (ARGV[0])

        case action
            when PLOT then
                if ARGV.length > 1
                    location = ARGV[1] if (ARGV[1])
                    date  = ARGV[2] if (ARGV[2])
                    month = date.split('-')[1].to_i
                    year  = date.split('-')[0].to_i
                    days  = days_in_month(year, month)
                    instance.plot_scale_data(location, date, days)
                else
                    puts "\nERROR: \"action=plot flags needs and argument\"\n\n"
                end
            when WRITE_TO_DB
                record_number = 0
                instance.write_scale_info(record_number)
            when DATA then
                if ARGV[1] =~ /retrieve/
                    record_number = 0
                    data = instance.get_scale_hash(record_number)
                    puts data
                else
                    puts "invalid data option"
                end
            when DUMP then
                parameter = ARGV[1] if (ARGV[1])
                case parameter
                    when 'scale' then
                        instance.dump_raw_scale_data
                    else
                        puts "Dump parameter not recognized"
                end
            when SEND_EMAIL then
                period = ARGV[1] if (ARGV[1])
                instance.mail_plot(period)
            else
                puts "Invalid info option: #{action}"

        end

    else
        puts "\n Not enough arguments "
        usage
    end
end
#-------------------------------------------------------

start_test
